  // page title = baseTile + content title
config.baseTitle = 'crazyeights api'
config.siteName = 'crazyeights api'
config.siteDes = 'crazyeights api'
config.siteKey = 'crazyeights'

config.logoUrl = 'http://truekingnet.gitee.io/crazyeights/'
config.assetBasePath = '/crazyeights/'

config.lang = 'zh-CN'
config.langs = ["en-US", "zh-CN"]

config.docProject = 'truekingnet/CrazyEights/'
//   // e.g https://raw.githubusercontent.com/{astaxie/build-web-application-with-golang}/master/{beanfactory.md}
  config.dataUrl = ''
// config.docUrl = 'https://github.com/beego/beedoc'
// config.editUrl = 'https://github.com/beego/beedoc/edit/master'

config.project = 'truekingnet/CrazyEights/doc/'
  config.projectUrl = 'https://gitee.com/truekingnet/CrazyEights'
// config.issueUrl = 'https://github.com/astaxie/beego/issues'

config.authorName = 'astaxie'
config.authorPage = 'https://gitee.com/truekingnet/CrazyEights'

config.theme = 'paper'
config.codeTheme = 'github'
config.catelogPage = 'catelog.md'
config.defaultPage = 'README.md'
config.makeTOC = true
config.emptyData = 'No content to display!'

config.onContentWrited = function (contentDom) {
  // contentDom.find('h1').first().prev('h2').remove()
  contentDom.find('h1').first().prevAll().remove()
}
